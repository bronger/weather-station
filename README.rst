Weather station
===============

This service posts the current weather data for Aachen to the MQTT topic
“wilson/weather/aachen/current”, and history and forecasts to
“wilson/weather/aachen/series”.


Data structure
--------------

One dataset looks like this::

  {
    "time":                  1708196293000,
    "relative_humidity":     1.0,
    "temperature":           1.0,
    "pressure":              1.0,
    "dew_point_temperature": 1.0,
    "absolute_humidity":     1.0,
  }

This is the format sent to “wilson/weather/aachen/current”.

For “wilson/weather/aachen/series”, a list of the above datasets is posted.  It
is guaranteed that the “time” is ascending, however, it is not guaranteed that
the datasets are equidistant in time.  Note that the current time might be
between the extreme ends of that series, of “close” to one of them.


Time series file
----------------

You must set the environment variable ``WEATHER_DATA_ROOT`` to an existing
directory.  In this directory, the file ``orsbach.dat`` is filled with a
weather data time series.

Note that the timestamps in the file are the UNIX Epoch in seconds, not in
milliseconds as in the MQTT messages.


Todos
-----

- Make program configurable with a configuration file
