package main

import (
	"errors"
	"io/fs"
	"os"
	"path/filepath"
)

var (
	dataRoot = os.Getenv("WEATHER_DATA_ROOT")
	file     *os.File
)

func fileSize(path string) (int64, error) {
	if fileInfo, err := os.Stat(path); err != nil {
		if errors.Is(err, fs.ErrNotExist) {
			return 0, nil
		} else {
			return 0, err
		}
	} else {
		return fileInfo.Size(), nil
	}
}

func writeToFile(line string) error {
	if file == nil {
		path := filepath.Join(dataRoot, "orsbach.dat")
		var err error
		if file, err = os.OpenFile(path, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666); err != nil {
			return err
		}
		if size, err := fileSize(path); err != nil {
			return err
		} else if size == 0 {
			if _, err := file.WriteString(
				"# 1. timestamp\n# 2. φ\n# 3. ϑ in ℃\n# 4. p/Pa\n" +
					"# 5. sunshine time share\n# 6. ρ_w/(kg/m³)\n# 7. sunlight power/(W/m²)\n" +
					"# 8. gust speed/(m/s)\n" +
					"# 9. probabilty of 0.5mm precipitation during last hour\n" +
					"# 10. probabilty of 3.0mm precipitation during last hour\n" +
					"# 11. total precipitation during the last hour in kg/m²\n"); err != nil {
				return err
			}
		}
	}
	if _, err := file.WriteString(line); err != nil {
		return err
	}
	return nil
}
