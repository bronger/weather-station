package main

import (
	"archive/zip"
	"bytes"
	"context"
	"encoding/xml"
	"errors"
	"fmt"
	"io"
	"math"
	"net/http"
	"strconv"
	"strings"
	"sync"
	"time"

	"smarthome-lib/weather"

	"golang.org/x/text/encoding/ianaindex"

	"gitlab.com/bronger/mqtt"
	tbr_logging "gitlab.com/bronger/tools/logging"
	"go4.org/must"
)

var (
	series     []weather.Dataset
	seriesLock sync.RWMutex
)

const orsbach = "10505"

func seriesUpdate(ctx context.Context, logger tbr_logging.Logger) (err error) {
	var w weatherData
	w, err = getWeatherData(ctx, orsbach)
	if err != nil {
		logger.Warn("Could not poll DWD", "error", err)
		return
	}
	if len(w.TimeSteps) == 0 {
		return errors.New("DWD weather data is empty")
	}
	func() {
		seriesLock.Lock()
		defer seriesLock.Unlock()
		forecastStart := w.TimeSteps[0].UnixMilli()
		for i, dataset := range series {
			if dataset.Time >= forecastStart {
				logger.Debug("Drop weather series elements that will be replaced",
					"number", len(series)-i, "deletion from", w.TimeSteps[0])
				series = series[:i]
				break
			}
		}
		logger.Debug("Append new weather series elements",
			"number", len(w.TimeSteps), "new elements start at", w.TimeSteps[0])
		for i, t := range w.TimeSteps {
			ϑ := w.values["TTT"][i] - 273.16
			τ := w.values["Td"][i] - 273.16
			φ := calc_φ(τ, ϑ)
			data := weather.Dataset{
				Time:     t.UnixMilli(),
				T:        ϑ,
				RelHum:   φ,
				Pressure: w.values["PPPP"][i],
				AbsHum:   calc_ρ_w(φ, ϑ),
				Sun:      w.values["SunD1"][i] / 3600,
				Irrad:    w.values["Rad1h"][i] * 1000 / 3600,
				Gust:     w.values["FX1"][i],
				Rain: weather.RainDataset{
					P05: w.values["R105"][i] / 100,
					P30: w.values["R130"][i] / 100,
					Q:   w.values["RR1c"][i],
				},
			}
			series = append(series, data)
		}
	}()
	pruneSeries(logger)
	checkSeries()
	seriesLock.RLock()
	defer seriesLock.RUnlock()
	return mqtt.PublishMsgWithOptions(ctx, logger, "wilson/weather/aachen/series", normalizeForJson(logger, series),
		mqtt.Options{QoS: 0, Retain: true})
}

func currentUpdate(ctx context.Context, logger tbr_logging.Logger) error {
	seriesLock.RLock()
	defer seriesLock.RUnlock()
	if current, err := weather.Current(series); err != nil {
		return err
	} else {
		errWrite := writeToFile(fmt.Sprintf("%v %v %v %v %v %v %v %v %v %v %v\n", current.Time/1000,
			current.RelHum, current.T, current.Pressure, current.Sun, current.AbsHum, current.Irrad,
			current.Gust, current.Rain.P05, current.Rain.P30, current.Rain.Q))
		errPublish := mqtt.PublishMsgWithOptions(ctx, logger, "wilson/weather/aachen/current", current,
			mqtt.Options{QoS: 0, Retain: true})
		if errWrite != nil {
			return errWrite
		}
		return errPublish
	}
}

func pruneSeries(logger tbr_logging.Logger) {
	seriesLock.Lock()
	defer seriesLock.Unlock()
	now := time.Now().UnixMilli()
	for i, data := range series {
		if i > 0 && series[i-1].Time >= data.Time {
			panic("Time series is not in chronological order")
		}
		if now-data.Time < 3600_000*24*2 {
			if i > 0 {
				logger.Debug("Prune series in memory", "deleted", i)
				series = series[i:]
			}
			break
		}
	}
	logger.Debug("Prune series in memory", "deleted", 0)
}

func checkSeries() {
	seriesLock.RLock()
	defer seriesLock.RUnlock()
	var lastestTimestamp int64
	for _, data := range series {
		if data.Time <= lastestTimestamp {
			panic("Invalid weather series")
		}
		lastestTimestamp = data.Time
	}
}

type weatherData struct {
	TimeSteps []time.Time `xml:"Document>ExtendedData>ProductDefinition>ForecastTimeSteps>TimeStep"`
	Forecast  []struct {
		Name  string `xml:"elementName,attr"`
		Value string `xml:"value"`
	} `xml:"Document>Placemark>ExtendedData>Forecast"`
	values map[string][]float64
}

// getWeatherData returns the weather data for the given WMO station ID (see
// https://opendata.dwd.de/climate_environment/CDC/help/stations_list_CLIMAT_data.txt).
func getWeatherData(ctx context.Context, station string) (w weatherData, err error) {
	weatherUrl := fmt.Sprintf("https://opendata.dwd.de/weather/local_forecasts/mos/MOSMIX_L/single_stations/"+
		"%[1]s/kml/MOSMIX_L_LATEST_%[1]s.kmz", station)
	if req, err := http.NewRequestWithContext(ctx, http.MethodGet, weatherUrl, nil); err != nil {
		return w, err
	} else if resp, err := http.DefaultClient.Do(req); err != nil {
		return w, err
	} else {
		defer must.Close(resp.Body)
		if content, err := io.ReadAll(resp.Body); err != nil {
			return w, err
		} else if r, err := zip.NewReader(bytes.NewReader(content), int64(len(content))); err != nil {
			return w, err
		} else if len(r.File) != 1 {
			return w, fmt.Errorf("Invalid number of files in KMZ file: %d", len(r.File))
		} else if f, err := r.Open(r.File[0].Name); err != nil {
			return w, err
		} else {
			defer must.Close(f)
			if content, err := io.ReadAll(f); err != nil {
				return w, err
			} else {
				decoder := xml.NewDecoder(bytes.NewReader(content))
				decoder.CharsetReader = func(charset string, reader io.Reader) (io.Reader, error) {
					if enc, err := ianaindex.IANA.Encoding(charset); err != nil {
						return nil, fmt.Errorf("Charset %s: %w", charset, err)
					} else if enc == nil {
						return nil, fmt.Errorf("Charset %s not supported", charset)
					} else {
						return enc.NewDecoder().Reader(reader), nil
					}
				}
				if err := decoder.Decode(&w); err != nil {
					return w, err
				}
				w.values = make(map[string][]float64)
				for _, item := range w.Forecast {
					for _, valueRaw := range strings.Fields(item.Value) {
						var (
							value float64
							err   error
						)
						if valueRaw == "-" {
							value = math.NaN()
						} else if value, err = strconv.ParseFloat(valueRaw, 64); err != nil {
							return w, fmt.Errorf("Could not parse %s: %w", item.Name, err)
						}
						w.values[item.Name] = append(w.values[item.Name], value)
					}
				}
				for name, values := range w.values {
					numberTS := len(w.TimeSteps)
					if len(values) != numberTS {
						return w, fmt.Errorf("Number of values %d does not match number "+
							"of time steps %d for field “%s”", len(values), numberTS, name)
					}
				}
				return w, nil
			}
		}
	}
}

// normalizeForJson replaces NaN values with JSON-serialisable float values
// like 0 or −1.  Currently, it does this only with rain data but NaN might
// occur also in other quantities.  See also
// https://github.com/golang/go/discussions/63397 for a possible future
// solution.
func normalizeForJson(logger tbr_logging.Logger, input []weather.Dataset) (output []weather.Dataset) {
	now := time.Now().UnixMilli()
	for _, ds := range input {
		daysInFuture := float64(ds.Time-now) / 3600_000 / 24
		var log func(msg string, args ...any)
		if daysInFuture < 2 {
			log = logger.Warn
		} else {
			log = logger.Debug
		}
		if math.IsNaN(ds.Rain.P05) {
			log("Replaced NaN with 0 for JSON serialising", "quantity", "R105",
				"days in future", daysInFuture)
			ds.Rain.P05 = 0
		}
		if math.IsNaN(ds.Rain.P30) {
			logger.Debug("Replaced NaN with 0 for JSON serialising", "quantity", "R130",
				"days in future", daysInFuture)
			ds.Rain.P30 = 0
		}
		if math.IsNaN(ds.Rain.Q) {
			logger.Debug("Replaced NaN with 0 for JSON serialising", "quantity", "RR1c",
				"days in future", daysInFuture)
			ds.Rain.Q = 0
		}
		output = append(output, ds)
	}
	return
}
