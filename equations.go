package main

import "math"

const (
	K2  = 17.62
	K3  = 243.12
	R_w = 461.52
)

// calc_τ returns the dew point temperature in ℃ for the given relative
// humidity φ (between 0 and 1) and temperature ϑ in ℃.
func calc_τ(φ, ϑ float64) float64 {
	// https://de.wikipedia.org/wiki/Taupunkt#Abh%C3%A4ngigkeit_der_Taupunkttemperatur_von_relativer_Luftfeuchtigkeit_und_Lufttemperatur
	return K3 * ((K2*ϑ)/(K3+ϑ) + math.Log(φ)) / ((K2*K3)/(K3+ϑ) - math.Log(φ))
}

// calc_ρ_w returns the absolute humidity in kg/m³ for the given relative
// humidity φ (between 0 and 1) and temperature ϑ in ℃.
func calc_ρ_w(φ, ϑ float64) (ρ_w float64) {
	T := ϑ + 273.15
	// https://de.wikipedia.org/wiki/S%C3%A4ttigungsdampfdruck#Berechnung_des_S%C3%A4ttigungsdampfdrucks_von_Wasser_%C3%BCber_die_Magnus-Formel
	E_w := 611.2 * math.Exp(K2*ϑ/(K3+ϑ))
	// https://de.wikipedia.org/wiki/S%C3%A4ttigung_(Physik)#S%C3%A4ttigung_von_Gasen_am_Beispiel_des_Wasserdampfs
	ρ_w_max := E_w / (R_w * T)
	// https://de.wikipedia.org/wiki/Luftfeuchtigkeit#Relative_Luftfeuchtigkeit
	ρ_w = φ * ρ_w_max
	return
}

// calc_φ returns the relative humidity (between 0 and 1) for the given dew
// point τ in ℃ and temperature ϑ in ℃.
func calc_φ(τ, ϑ float64) float64 {
	// https://de.wikipedia.org/wiki/Taupunkt#Abh%C3%A4ngigkeit_der_Taupunkttemperatur_von_relativer_Luftfeuchtigkeit_und_Lufttemperatur
	return math.Exp((τ/K3*(K2*K3)/(K3+ϑ) - (K2*ϑ)/(K3+ϑ)) / (τ/K3 + 1))
}
