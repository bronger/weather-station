module gitlab.com/bronger/weather-station

go 1.22.0

require (
	gitlab.com/bronger/mqtt v0.0.0-20240521111219-3c3d37c7dc58
	gitlab.com/bronger/tools v0.0.0-20240518054958-f26ea481e984
	go4.org v0.0.0-20230225012048-214862532bf5
	golang.org/x/text v0.14.0
	smarthome-lib v0.0.0-00010101000000-000000000000
)

require (
	github.com/eclipse/paho.golang v0.21.1-0.20240519233048-68527a24dff0 // indirect
	github.com/gorilla/websocket v1.5.1 // indirect
	github.com/hablullah/go-juliandays v1.0.1-0.20220316153050-f56193695a5b // indirect
	github.com/hablullah/go-sampa v1.0.0 // indirect
	golang.org/x/net v0.21.0 // indirect
)

replace smarthome-lib => ../smarthome-lib
