package main

import (
	"bufio"
	"context"
	"encoding/gob"
	"errors"
	"fmt"
	"log/slog"
	"os"
	"os/signal"
	"path/filepath"
	"sync"
	"syscall"
	"time"

	"gitlab.com/bronger/mqtt"
	tbr_errors "gitlab.com/bronger/tools/errors"
	tbr_logging "gitlab.com/bronger/tools/logging"
	"go4.org/must"
)

func init() {
	tbr_logging.Init(os.Stderr, slog.LevelDebug)
}

func mainLoop(ctx context.Context, logger tbr_logging.Logger, wg *sync.WaitGroup, errs chan<- error) {
	defer wg.Done()
	seriesTicker := time.Tick(time.Hour)
	if err := seriesUpdate(ctx, logger.With("component", "series update")); err != nil {
		errs <- err
		return
	}
	currentTicker := time.Tick(10 * time.Minute)
	if err := currentUpdate(ctx, logger.With("component", "current update")); err != nil {
		errs <- err
		return
	}
	for {
		select {
		case <-seriesTicker:
			if err := seriesUpdate(ctx, logger.With("component", "series update")); err != nil {
				errs <- err
				return
			}
		case <-currentTicker:
			if err := currentUpdate(ctx, logger.With("component", "current update")); err != nil {
				errs <- err
				return
			}
		case <-ctx.Done():
			return
		}
	}
}

// errs transmits errors.  Unless it is not a cancelled context, they are
// considered fatal and lead to an immediate termination of the program.
var errs = make(chan error)

func main() {
	baseCtx, baseCtxStop := signal.NotifyContext(context.Background(), syscall.SIGTERM, syscall.SIGINT)
	logger := slog.Default()
	readState(logger)
	pruneSeries(logger)
	checkSeries()
	go func() {
		for err := range errs {
			if errors.Is(err, context.Canceled) {
				slog.Warn("Main loop reported “context cancelled”.  Ignoring")
			} else {
				tbr_errors.ExitWithExpectedError("Fatal error in main loop", 10, "error", err)
			}
		}
	}()
	mqtt.DefaultClient = mqtt.GetMQTTClient(baseCtx, logger.With("component", "MQTT"), "tcp://192.168.178.30:1883",
		"weather", nil)
	var wg sync.WaitGroup
	wg.Add(1)
	go mainLoop(baseCtx, logger.With("component", "main loop"), &wg, errs)
	<-baseCtx.Done()
	close(errs)

	logger.Info("Shutting down …")
	mqtt.WaitForShutdown()
	wg.Wait()
	storeState(logger)
	baseCtxStop()
	logger.Info("Shutdown completed")
}

func storeState(logger tbr_logging.Logger) {
	dir := os.Getenv("WEATHER_DATA_ROOT")
	if dir == "" {
		return
	}
	path := filepath.Join(dir, "series.gob")
	if f, err := os.Create(path + ".partial"); err != nil {
		tbr_errors.ExitOnExpectedError(err, "Could not store series data", 10, "path", path+".partial")
	} else {
		func() {
			defer must.Close(f)
			w := bufio.NewWriter(f)
			defer must.Do(w.Flush)
			e := gob.NewEncoder(w)
			if err := e.Encode(series); err != nil {
				panic(fmt.Errorf("Could not encode series data: %w", err))
			}
		}()
		if err := os.Rename(path+".partial", path); err != nil {
			tbr_errors.ExitOnExpectedError(err, "Could not rename series data file", 10,
				"from", path+".partial", "to", path)
		}
		logger.Debug("Wrote series", "path", path, "number", len(series))
	}
}

func readState(logger tbr_logging.Logger) {
	dir := os.Getenv("WEATHER_DATA_ROOT")
	path := filepath.Join(dir, "series.gob")
	if f, err := os.Open(path); err == nil {
		defer must.Close(f)
		d := gob.NewDecoder(f)
		err := d.Decode(&series)
		tbr_errors.ExitOnExpectedError(err, "Could not decode series data", 10)
		logger.Debug("Read series", "path", path, "number", len(series))
	}
}
